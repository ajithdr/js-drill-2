/*  ==== Problem #4 ====
    The accounting team needs all the years from every car on the lot. 
    Execute a function that will return an array from the dealer data containing 
    only the car years and log the result in the console as it was returned.
*/

const inventory = require('./inventory.cjs');

function problem4(inventory) {

    if(arguments.length < 1 || !Array.isArray(inventory)) {
        return [];
    }

    const carYears = inventory.map(car => {
        if(car.hasOwnProperty('car_year')) {
            return car.car_year;
        }
    });

    return carYears;
}

// const result = problem4(inventory);
// console.log(result);

module.exports = problem4;